module.exports = {
    title: 'Ember documentation',
    description: 'Installation and configuration instructions for Ember',
    evergreen: true,
    dest: 'public',
    plugins: ['@vuepress/medium-zoom'],
    head: [['link', { rel: 'icon', href: '/ember.ico' }]],
    themeConfig: {
        repo: 'http://gitlab.com/kekalainen/ember-issues',
        repoLabel: 'Issue tracker',
        docsRepo: 'http://gitlab.com/kekalainen/ember-docs',
        docsDir: 'docs',
        smoothScroll: true,
        editLinks: true,
        lastUpdated: true,
        nav: [{ text: 'Discord', link: 'https://discord.gg/g5C4SzT' }],
        sidebar: {
            '/': [
                {
                    title: 'Installation',
                    collapsable: false,
                    sideBarDepth: 2,
                    children: [
                        'installation/web',
                        'installation/gmod',
                        'installation/rust',
                        'installation/forums',
                        'installation/discord'
                    ]
                },
                {
                    title: 'Updating',
                    collapsable: false,
                    children: [
                        'updating/web',
                        'updating/gmod',
                        'updating/rust',
                        'updating/forums',
                        'updating/discord'
                    ]
                },
                {
                    title: 'Configuration',
                    collapsable: false,
                    children: [
                        'configuration/cache',
                        'configuration/localization',
                        'configuration/oauth',
                        'configuration/roles-permissions',
                        'configuration/server',
                        'configuration/package',
                        'configuration/bitpay',
                        'configuration/paypal',
                        'configuration/stripe',
                        'configuration/webhooks'
                    ]
                },
                {
                    title: 'Extending',
                    collapsable: false,
                    children: ['extending/modules', 'extending/overriding']
                },
                {
                    title: 'Miscellaneous',
                    collapsable: false,
                    children: [
                        'miscellaneous/cli',
                        'miscellaneous/troubleshooting'
                    ]
                }
            ]
        }
    }
};