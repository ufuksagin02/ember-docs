# Servers

- Navigate to **Admin** > **Servers** and click on the **+** icon on the top left of the server manager.
- Enter a name for the server and press **OK**.
- More input fields will appear. Populate the fields as described below.

## Details
- Enter a descriptive name for the server, such as `DarkRP`.
- Enter the public IP address or domain of the server. Required for server queries and RCON, if enabled.
- Enter the server's game port and query port.
- Specify the game the server is running.
- Optionally configure a custom cover image for the landing page server card.

## Integration
- Generate a token for the server.
- Specify the polling interval in seconds. This sets the frequency at which new purchases and bans are checked for.
- Select the scope of the bans that should be enforced on the server.
- Select the role sync method to be used on the server.
- Enable ban log if desired.

::: warning
Changes to integration settings take effect only when the addon / plugin is reloaded.
:::

## Remote console
If the RCON configuration options are filled, a remote console component will appear below the server manager. Ember will also utilize the RCON connection behind the scenes to instantly assign store package roles in-game and enforce server-scoped bans issued from the web interface.

The connection can be tested by sending a command (for example `status`).

## Example configuration
![](../media/screenshots/server_manager.jpg)
