# Ember documentation

This is the documentation for [Ember](https://www.gmodstore.com/scripts/view/5620).

Here you will find instructions for installing, updating, configuring and extending Ember and its official modules and integrations.

::: tip
These instructions assume the reader knows the basics of working with their hosting environment, e.g. Linux, FTP, a web server, PHP and MySQL.

There are plenty of guides online for each.
:::

## Requirements

* a web server capable of URL rewriting and executing PHP scripts
* PHP 7.4 or above
* MySQL 5.7.23 or above.
