# Forums DLC

## Preparation

Ensure you have the latest version of [Ember](https://www.gmodstore.com/scripts/view/5620) installed.

## Removing the old files

Prior to updating, remove old files that you haven't modified to ensure a clean installation. As in, the entire `modules/forums` subdirectory.

## Downloading the new files

Download the new files from [GmodStore](https://www.gmodstore.com/scripts/view/5876).

## Applying the update

Upload the new files to your web server. To finish the update browse to your Ember installation with a web browser. A database update is performed automatically if necessary.
