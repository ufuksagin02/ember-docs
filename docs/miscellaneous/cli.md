# Command line interface

The `cli` file is a PHP script which is used to run Ember's command line interface. It can be executed using [PHP from the command line](https://www.php.net/manual/en/features.commandline.usage.php) in the directory in which Ember is installed.

## Listing commands

```php
php cli
```

## Maintenance mode

### Entering

```php
php cli down
```

### Exiting

```php
php cli up
```

::: warning
Database migrations are not run automatically when maintenance mode is active.
:::

## Database migrations

### Running manually

```php
php cli db:migrate
```
